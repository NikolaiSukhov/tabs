

//ВАРІАНТ 1 - 
// let getElem = document.querySelector('.par_1')
// let getElems = document.querySelector('.par_2')
// let getElems2 = document.querySelector('.par_3')
// let getElems3 = document.querySelector('.par_4')
// let getElems4 = document.querySelector('.par_5')
// console.log(getElems);
// let button = document.querySelector(".tabs-title1").addEventListener('click', function(){ getElem.style.display = "block";getElems.style.display = "none"; getElems2.style.display = "none"; getElems3.style.display = "none"; getElems4.style.display = "none";; })
// let button2 = document.querySelector(".tabs-title2").addEventListener('click', function(){ getElems.style.display = "block"; getElem.style.display = "none"; getElems2.style.display = "none"; getElems3.style.display = "none"; getElems4.style.display = "none";})
// let button3 = document.querySelector(".tabs-title3").addEventListener('click', function(){ getElems2.style.display = "block";getElem.style.display = "none"; getElems.style.display = "none"; getElems3.style.display = "none"; getElems4.style.display = "none"; })
// let button4 = document.querySelector(".tabs-title4").addEventListener('click', function(){ getElems3.style.display = "block";getElem.style.display = "none"; getElems2.style.display = "none";  getElems4.style.display = "none"; })
// let button5 = document.querySelector(".tabs-title5").addEventListener('click', function(){ getElems4.style.display = "block"; getElem.style.display = "none"; getElems2.style.display = "none"; getElems3.style.display = "none";})
// console.log(button);

//ВАРІАНТ 2 -
const tabs = document.querySelectorAll(".tabs li");
const paragraphs = document.querySelectorAll(".tabs-content li");

tabs.forEach((tab, index) => {
  tab.addEventListener("click", () => {
    paragraphs.forEach((paragraph, paragraphIndex) => {
      if (index === paragraphIndex) {
        paragraph.style.display = "block";
      } else {
        paragraph.style.display = "none";
      }
    });
  });
});